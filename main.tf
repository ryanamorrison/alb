terraform {
  required_version = ">= 1.0.0, < 2.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

resource "aws_lb" "lb" {
  name = var.alb_name
  load_balancer_type = "application"
  subnets = var.subnets_ids
  security_groups = [aws_security_group.alb-sg.id]
}

resource "aws_lb_listener" "web" {
  load_balancer_arn = aws_lb.lb.arn
  port = local.http_port 
  protocol = var.web_proto 

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code = 404
    }
  }
}

resource "aws_security_group" "alb-sg" {
  name = var.alb_name
}
