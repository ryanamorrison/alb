variable "alb_name" {
  description = "name for the ALB"
  type = string
}

variable "subnets_ids" {
  description = "the subnet ids to deploy to"
  type = list(string)
}

variable "web_proto" {
  description = "web protocol to use"
  type = string
  default = "HTTP"
}

