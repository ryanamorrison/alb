output "alb_dns_name" {
  value = aws_lb.lb.dns_name
  description = "domain name of the load balancer"
}

output "alb_http_listener_arn" {
  value = aws_lb_listener.web.arn
  description = "ARN of the web listener"
}

output "alb_security_group_id" {
  value = aws_security_group.alb-sg.id
  description = "the id of the SG attached to the load balancer"
}
